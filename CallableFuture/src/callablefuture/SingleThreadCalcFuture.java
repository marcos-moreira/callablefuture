/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package callablefuture;

/**
 *
 * @author avilapm
 */
public class SingleThreadCalcFuture {

    public static void main(String[] args) {
        double sum = 0;

        long startTime = System.nanoTime();
        for (int i = 1; i < 1E8; i++) {
            sum += i;
            Math.sin(sum);
        }

        System.out.println("Sum = " + sum);

        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.println(totalTime/1E6);
    }

}
